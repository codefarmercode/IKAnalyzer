package org.huidao.lucene.analysis.ik;

import java.io.Reader;
import java.util.Map;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.ngram.EdgeNGramTokenFilter;
import org.apache.lucene.analysis.ngram.EdgeNGramTokenizer;
import org.apache.lucene.analysis.util.TokenizerFactory;
import org.apache.lucene.util.AttributeFactory;
import org.wltea.analyzer.lucene.IKTokenizer;

/**
 * 汇道科技
 * 
 * @author 陈吉林
 * @date 创建于：2017年4月1日 上午10:28:10
 */
public class IKTokenizerFactory extends TokenizerFactory {
	private final boolean useSmart;

	/**
	 * 支持solr的schema.xml文件中配置智能分词功能！！
	 * 
	 * <fieldType name="text_ik" class="solr.TextField"> 
	 *  <analyzer type="index"> <tokenizer
	 * class="org.huidao.lucene.analysis.ik.IKTokenizerFactory"
	 * useSmart="false"/> </analyzer> <analyzer type="query"> <tokenizer
	 * class="org.huidao.lucene.analysis.ik.IKTokenizerFactory" useSmart="true"
	 * /> </analyzer> </fieldType>
	 * 
	 * @param args
	 */
	public IKTokenizerFactory(Map<String, String> args) {
		super(args);
		useSmart = getBoolean(args, "useSmart", false);
		if (!args.isEmpty()) {
			throw new IllegalArgumentException("Unknown parameters: " + args);
		}
	}

	@Override
	public Tokenizer create(AttributeFactory factory, Reader input) {
		Tokenizer ik = new IKTokenizer(input, this.useSmart);
		return ik;
	}

}
