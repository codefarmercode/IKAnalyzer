# IKAnalyzer

#### 项目介绍
在原来的2012版本上增加在solr中配置智能分词功能！！

#### 使用说明
在服务器solr的schema.xml配置文件中添加如下配置，
即可实现索引时最大化分词，而查询时智能分词功能！！
	 
	<fieldType name="text_ik" class="solr.TextField"> 
	    <analyzer type="index"> 
            <tokenizer class="org.huidao.lucene.analysis.ik.IKTokenizerFactory" useSmart="false"/>
        </analyzer> 
        <analyzer type="query"> 
            <tokenizer class="org.huidao.lucene.analysis.ik.IKTokenizerFactory" useSmart="true"/>
        </analyzer> 
    </fieldType>